﻿using Microsoft.ML.Data;
/// <summary>
/// Класс входных данных, содержит свойства, в которых будут
/// хранится сетевые параметры атаки.
/// </summary>
internal class Issue
{
    [LoadColumn(0)]
    public float Unnamed { get; set; }

    [LoadColumn(1)]
    [ColumnName("Flow ID")]
    public string Flow_ID { get; set; }

    [LoadColumn(2)]
    [ColumnName("Source IP")]
    public string Source_IP { get; set; }

    [LoadColumn(3)]
    public float Source_Port { get; set; }

    [LoadColumn(4)]
    [ColumnName("Destination IP")]
    public string Destination_IP { get; set; }

    [LoadColumn(5)]
    public float Destination_Port { get; set; }

    [LoadColumn(6)]
    public float Protocol { get; set; }

    [LoadColumn(7)]
    [ColumnName("Timestamp")]
    public string Timestamp { get; set; }

    [LoadColumn(8, 86)]
    [VectorType(79)]
    public float[] Characteristics { get; set; }

    [LoadColumn(87)]
    public string Expected { get; set; }

}