﻿using Microsoft.ML;
using Microsoft.ML.Data;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Library
{
    /// <summary>
    /// Класс классификации.
    /// </summary>
    public static class Analyse
    {
        public static IDataView analysedDataView;

        /// <summary>
        /// Путь к готовой модели нейронной сети.
        /// </summary>
        private static string ModelPath =>
        Path.Combine("model2.zip");

        public static FileStream model;

        /// <summary>
        /// Метод классификации входных данных.
        /// </summary>
        /// <param name="analysedDataPath">Путь к анализируемому документу.</param>
        /// <returns>Список названий идентифицированных атак.</returns>
        public static List<string> PredictIssue(string analysedDataPath)
        {
            IEnumerable<string> labelsColumn;
            var _mlContext = new MLContext();

            // Поток для считывания модели.
            model = new FileStream(ModelPath, FileMode.Open);

            try
            {
                // Загрузка модели.
                ITransformer loadedModel = _mlContext.Model
                     .Load(model, out _);

                model.Close();

                // Загрузка входных данных.
                analysedDataView = _mlContext.Data.
                    LoadFromTextFile<Issue>(analysedDataPath,
                    separatorChar: ',', hasHeader: true);

                // Классификация.
                IDataView predictions = loadedModel.Transform(analysedDataView);

                // Перечисление названий идентифицированных атак.
                labelsColumn = predictions.GetColumn<string>("PredictedLabel");

                // Приведение названий к удобочитаемому виду.
                var labels = labelsColumn.ToList();
                for (int i = 0; i < labels.Count; i++)
                    if (labels[i].Contains("DrDoS_"))
                        labels[i] = labels[i].Remove(0, 6);

                return labels;
            }
            catch (System.Reflection.TargetInvocationException)
            {
                // Срабатывает при отмене операции классификации
                return null;
            }
            // ОБработать ситуацию, когда модель не найдена
            catch (FileNotFoundException ex)
            {
                return null;
            }
           

        }
    }
}
