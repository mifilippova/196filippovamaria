﻿using Microsoft.ML;
using System;
using System.IO;

namespace NeuralNetwork
{
    class Program
    {
        #region Global variables

        /// <summary>
        /// Путь к данным для обучения нейронной сети.
        /// </summary>
        private static string _trainDataPath
            => Path.Combine("..", "..", "..", "..", "..", "Data", "CSV-data",
                 "*");

        /// <summary>
        /// Путь к данным для оценки нейронной сети.
        /// </summary>
        private static string _testDataPath =>
            Path.Combine("..", "..", "..", "..", "..",
                "Data", "Test", "*");
        /// <summary>
        /// Путь к сохраняемой модели нейронной сети.
        /// </summary>
        private static string _modelPath =>
            Path.Combine("..", "..", "..", "..", "LbfgsModel", "model.zip");

        private static MLContext _mlContext;

        // Для единичных предсказаний.
        private static PredictionEngine<Issue, IssuePrediction> _predEngine;

        private static ITransformer _trainedModel;
        static IDataView _trainingDataView;
        #endregion
        static void Main()
        {
            #region Teaching

            _mlContext = new MLContext(seed: 0);

            // Загрузка данных для обучения.
            _trainingDataView = _mlContext.Data.
                LoadFromTextFile<Issue>(_trainDataPath,
                separatorChar: ',', hasHeader: true);

            // Подготовка данных для обучения.
            var pipeline = ProcessData();

            // Создание модели.
            var trainingPipeline =
                BuildAndTrainModel(_trainingDataView, pipeline);

            // Оценка модели.
            Evaluate(_trainingDataView.Schema);
            #endregion


        }



        /// <summary>
        /// Loads the saved model
        /// Creates a single issue of test data.
        /// Predicts Attack based on test data.
        /// Combines test data and predictions for reporting.
        /// Displays the predicted results.
        /// </summary>
        private static void PredictIssue()
        {
            ITransformer loadedModel = _mlContext.Model
                 .Load(_modelPath, out var modelInputSchema);

            var issue = new Issue()
            {
                Unnamed = 24f,
                Flow_ID = "192.168.50.254-224.0.0.5-0-0-0",
                Source_IP = "192.168.50.254",
                Source_Port = 0f,
                Destination_IP = "224.0.0.5",
                Destination_Port = 0f,
                Protocol = 0f,
                Timestamp = "03.11.2018 9:18:17",
                Characteristics = new[] { 114456999f, 45f, 0f, 0.0f, 0.0f, 0.0f,
                0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.3931607537604581f,
            26012995.4318181816f, 4295632.000149808f, 10001143.0f, 1.0f, 114456999.0f,
            26012995.4318181816f, 4295632.000149808f, 10001143.0f, 1.0f, 0.0f,0.0f,0.0f,0.0f,0.0f,
            0f,0f,0f,0f,0f,0f,  0.3931607537604581f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f,
            0f,0f,0f,0f,0f,0f,0f,0f, 0.0f, 0.0f, 0.0f, 0.0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 45f,
            0f, 0f, 0f, -1f,-1f,0f,0f, 8185.5833333334f, 28337.11228778624f, 98168.0f, 3.0f,
            9529897.25f, 351582.63126924314f, 10001143.0f, 9048097.0f, 0f, 0f
            }
            };

            _predEngine = _mlContext.Model
                .CreatePredictionEngine<Issue, IssuePrediction>(loadedModel);

            var prediction = _predEngine.Predict(issue);
            Console.WriteLine($"=============== Single Prediction " +
                $"- Result: {prediction.Attack} ===============");

        }

        /// <summary>
        /// Creates the training algorithm class.
        /// Trains the model.
        /// Predicts area based on training data.
        /// </summary>
        /// <param name="trainingDataView"></param>
        /// <param name="pipeline"></param>
        /// <returns>Returns the model.</returns>
        private static IEstimator<ITransformer> BuildAndTrainModel(IDataView trainingDataView,
            IEstimator<ITransformer> pipeline)
        {
            var trainingPipeline = pipeline.Append(_mlContext.MulticlassClassification
                .Trainers.LbfgsMaximumEntropy("Label", "Features"))
                .Append(_mlContext.Transforms.Conversion.MapKeyToValue("PredictedLabel"));

            #region Model training
            DateTime start = DateTime.Now;
            Console.WriteLine($"====== Model training ========");

            _trainedModel = trainingPipeline.Fit(trainingDataView);

            Console.WriteLine($"=======  Finished Training the model " +
                $"Consumed time: {DateTime.Now - start} ==========");

            #endregion


            #region Single prediction
            Console.WriteLine("======= Single Prediction just-trained-model =========");

            _predEngine = _mlContext.Model
                .CreatePredictionEngine<Issue, IssuePrediction>(_trainedModel);
            var issue = new Issue()
            {
                Unnamed = 24f,
                Flow_ID = "192.168.50.254-224.0.0.5-0-0-0",
                Source_IP = "192.168.50.254",
                Source_Port = 0f,
                Destination_IP = "224.0.0.5",
                Destination_Port = 0f,
                Protocol = 0f,
                Timestamp = "03.11.2018 9:18:17",
                Characteristics = new[] { 114456999f, 45f, 0f, 0.0f, 0.0f, 0.0f,
                0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.3931607537604581f,
            26012995.4318181816f, 4295632.000149808f, 10001143.0f, 1.0f, 114456999.0f,
            26012995.4318181816f, 4295632.000149808f, 10001143.0f, 1.0f, 0.0f,0.0f,0.0f,0.0f,0.0f,
            0f,0f,0f,0f,0f,0f,  0.3931607537604581f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f,
            0f,0f,0f,0f,0f,0f,0f,0f, 0.0f, 0.0f, 0.0f, 0.0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 45f,
            0f, 0f, 0f, -1f,-1f,0f,0f, 8185.5833333334f, 28337.11228778624f, 98168.0f, 3.0f,
            9529897.25f, 351582.63126924314f, 10001143.0f, 9048097.0f, 0f, 0f
            }
            };

            var prediction = _predEngine.Predict(issue);

            Console.WriteLine($"=============== Single Prediction " +
                $"just-trained-model - Result: {prediction.Attack} ===============");

            #endregion

            SaveModelAsFile(_mlContext, _trainingDataView.Schema, _trainedModel);

            return trainingPipeline;
        }

        /// <summary>
        /// Loads the test dataset.
        /// Creates the multiclass evaluator.
        /// Evaluates the model and create metrics.
        /// Displays the metrics.
        /// </summary>
        /// <param name="trainingDataViewSchema"></param>
        private static void Evaluate(DataViewSchema trainingDataViewSchema)
        {
            var testDataView = _mlContext.Data
                .LoadFromTextFile<Issue>(_testDataPath, separatorChar: ',', hasHeader: true);

            var testMetrics = _mlContext.MulticlassClassification
                .Evaluate(_trainedModel.Transform(testDataView));

            #region Print metrix
            Console.WriteLine($"*************************************************************************************************************");
            Console.WriteLine($"*       Metrics for Multi-class Classification model - Test Data     ");
            Console.WriteLine($"*------------------------------------------------------------------------------------------------------------");
            Console.WriteLine($"*       MicroAccuracy:    {testMetrics.MicroAccuracy:0.###}");
            Console.WriteLine($"*       MacroAccuracy:    {testMetrics.MacroAccuracy:0.###}");
            Console.WriteLine($"*       LogLoss:          {testMetrics.LogLoss:#.###}");
            Console.WriteLine($"*       LogLossReduction: {testMetrics.LogLossReduction:#.###}");
            Console.WriteLine($"*************************************************************************************************************");
            #endregion

            #region Write metrix in file
            using (var fs = new FileStream(Path.Combine("..", "..", "..", "logs.txt"), FileMode.Append))
            {
                using (var sw = new StreamWriter(fs))
                {
                    sw.WriteLine(DateTime.Now);
                    sw.WriteLine($"MicroAccuracy: {testMetrics.MicroAccuracy:0.###}\n" +
                        $"MacroAccuracy: {testMetrics.MacroAccuracy:0.###}\n" +
                        $"LogLoss: {testMetrics.LogLoss}\n LogLossReduction{testMetrics.LogLossReduction:#.###}\n\r");
                }
            }
            #endregion
        }

        /// <summary>
        /// Serialize and store the trained model as a zip file
        /// </summary>
        /// <param name="mlContext"></param>
        /// <param name="trainingDataViewSchema"></param>
        /// <param name="model"></param>
        private static void SaveModelAsFile(MLContext mlContext,
            DataViewSchema trainingDataViewSchema, ITransformer model)
        {
            mlContext.Model.Save(model, trainingDataViewSchema,
                Path.Combine("..", "..", "..", "..", "LbfgsModel", "model2.zip"));

        }

        /// <summary>
        /// Extracts and transforms the data.
        /// </summary>
        /// <returns>Returns the processing pipeline.</returns>
        private static IEstimator<ITransformer> ProcessData()
        {
            Console.WriteLine($"======== Processing Data ========");
            var pipeline = _mlContext.Transforms.Conversion.
                MapValueToKey(inputColumnName: "Expected", outputColumnName: "Label")

            #region Featurisation of string data
            .Append(_mlContext.Transforms.Text.FeaturizeText(inputColumnName: "Flow ID", outputColumnName: "FeaturizedFlow ID"))
                .Append(_mlContext.Transforms.Text.FeaturizeText(inputColumnName: "Source IP", outputColumnName: "FeaturizedSource IP"))
                .Append(_mlContext.Transforms.Text.FeaturizeText(inputColumnName: "Destination IP", outputColumnName: "FeaturizedDestination IP"))
                .Append(_mlContext.Transforms.Text.FeaturizeText(inputColumnName: "Timestamp", outputColumnName: "FeaturizedTimestamp"))

                .Append(_mlContext.Transforms.Concatenate("Features", "FeaturizedFlow ID",
                "FeaturizedSource IP", "FeaturizedDestination IP", "FeaturizedTimestamp"));
            #endregion

            Console.WriteLine($"======= Finished Processing Data =======");

            ITransformer dataPrepTransformer = pipeline.Fit(_trainingDataView);

            Console.WriteLine("Saving data preparation transformer");
            // Save data preparation pipeline
            _mlContext.Model.Save(dataPrepTransformer, _trainingDataView.Schema,
                Path.Combine("..", "..", "..", "..", "LbfgsModel", "data_preparation_pipeline.zip"));
            Console.WriteLine("Data preparation transformer was successfully saved");

            return pipeline;

        }
    }
}
