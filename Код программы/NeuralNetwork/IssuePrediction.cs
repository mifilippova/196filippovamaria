﻿using Microsoft.ML.Data;

/// <summary>
/// Класс выходных данных.
/// </summary>
internal class IssuePrediction
{
    [ColumnName("PredictedLabel")]
    public string Attack;
}