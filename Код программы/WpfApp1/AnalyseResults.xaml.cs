﻿using Microsoft.ML;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace WpfApp1
{
    /// <summary>
    /// Логика взаимодействия для AnalyseResults.xaml
    /// </summary>
    public partial class AnalyseResults : Window
    {
        private static DataTable data;
        public AnalyseResults()
        {
            InitializeComponent();

            if (MainWindow.labels != null)
                PresentInfo();
        }

        /// <summary>
        /// Метод представляет информацию об атаках в табличной форме.
        /// </summary>
        private void PresentInfo()
        {
            data = ConvertCSVtoDataTable(MainWindow._analysedDataPath, MainWindow.labels);
            var view = data.AsDataView();
            attacksList.ItemsSource = view;
        }


        /// <summary>
        /// Счиитывает анализируемый CSV файл.
        /// Составляет таблицу данных, которая и будет отображаться на экране.
        /// </summary>
        /// <param name="strFilePath"></param>
        /// <param name="labels"></param>
        /// <returns></returns>
        private static DataTable ConvertCSVtoDataTable(string strFilePath, List<string> labels)
        {
            DataTable dt = new DataTable();
            var columns = new List<DataViewSchema.Column>(){
                Library.Analyse.analysedDataView.Schema["Flow ID"],
                Library.Analyse.analysedDataView.Schema["Source IP"],
                Library.Analyse.analysedDataView.Schema["Destination IP"],
                Library.Analyse.analysedDataView.Schema["Timestamp"]
            };

            var set = new List<string>() { "Flow ID", "Source IP", "Destination IP", "Timestamp" };
            try
            {
                using (StreamReader sr = new StreamReader(strFilePath))
                {
                    string[] headers = sr.ReadLine().Split(',');
                    for (int i = 0; i < headers.Length; i++)
                    {
                        headers[i] = headers[i].TrimStart(' ').TrimEnd(' ');
                    }

                    dt.Columns.Add("Идентификатор потока");
                    dt.Columns.Add("IP-адрес источника");
                    dt.Columns.Add("IP-адрес назначения");
                    dt.Columns.Add("Время");
                    dt.Columns.Add("Тип атаки", Type.GetType("System.String"));

                    int j = 0;
                    while (!sr.EndOfStream)
                    {
                        string[] rows = sr.ReadLine().Split(',');
                        DataRow dr = dt.NewRow();
                        for (int i = 0; i <= rows.Length; i++)
                        {
                            if (i == headers.Length)
                                dr[set.Count] = labels[j];
                            else if (set.Contains(headers[i]))
                            {
                                dr[set.IndexOf(headers[i])] = rows[i];
                            }

                        }
                        dt.Rows.Add(dr);
                        j++;
                    }

                }
                return dt;

            }
            catch (NullReferenceException)
            {
                MessageBox.Show($"Проиошла ошибка при считывании" +
                    $" анализируемого файла {strFilePath}");
                return dt;

            }
            catch (IOException)
            {
                MessageBox.Show($"Проиошла ошибка при считывании" +
                    $" анализируемого файла {strFilePath}");

                return dt;

            }

        }


        /// <summary>
        /// Метод сохранения файла с результатами анализа.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SaveAsFileDialog(object sender, RoutedEventArgs e)
        {
            SaveFileDialog dlg = new SaveFileDialog();
            dlg.FileName = "Analisys Results";

            dlg.DefaultExt = ".csv";
            // Расширение файла - CSV

            dlg.Filter = "Text documents (.csv)|*.csv";
            // Отображает только CSV файлы

            dlg.FileName = "Результаты классификации";

            Nullable<bool> result = dlg.ShowDialog();
            if (result == true)
            {
                try
                {
                    // Запись данных в файл.
                    using (var sw = new StreamWriter(dlg.FileName))
                    {
                        var lines = new List<string>();

                        var columnNames = new List<string>()
                        { "Flow ID", "Source IP", "Destination IP", "Timestamp", "Attack" };

                        var header = string.Join(",", columnNames
                            .Select(name => $"{name}"));

                        lines.Add(header);
                        var values = data.AsEnumerable().Select(row =>
                        string.Join(",", row.ItemArray.Select(val => $"{val}")));


                        lines.AddRange(values);
                        for (int i = 0; i < lines.Count; i++)
                            sw.WriteLine(lines[i]);
                    }
                }
                catch (Exception)
                {
                    MessageBox.Show("Произошла ошибка в записи результатов анализа.");
                }
            }
        }

        /// <summary>
        /// Отображает все атаки.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AllAttacks_Selected(object sender, RoutedEventArgs e)
        {
            attacksList.ItemsSource = data.AsDataView();
        }

        /// <summary>
        /// Отображает атаки только одного определенного вида,
        /// запрашиваемого оператором.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Attack_Selected(object sender, RoutedEventArgs e)
        {
            try
            {
                var attacks = from rows in data.AsEnumerable()
                              where rows.Field<string>("Тип атаки") == ((ListViewItem)sender).Content.ToString()
                              select rows;

                var view = attacks.AsDataView();
                attacksList.ItemsSource = view;
            }
            catch (ArgumentNullException)
            {
                MessageBox.Show("Нет данных для отображения");
            }
        }

        /// <summary>
        /// Возвращение на начальную страницу.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void File_Click(object sender, RoutedEventArgs e)
        {
            var main = new MainWindow();
            main.Show();
            Close();
        }
    }
}