﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Windows;

namespace WpfApp1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        /// <summary>
        /// Путь к анализируемому документу.
        /// </summary>
        public static string _analysedDataPath = default;

        /// <summary>
        /// Список названий идентифицированных атак.
        /// </summary>
        public static List<string> labels;

        /// <summary>
        /// Делегат, который будет содержать методы,
        /// исполняемые в потоке классификации.
        /// </summary>
        private static ThreadStart thSt;

        /// <summary>
        /// Поток для классификации.
        /// </summary>
        private static Thread worker;

        public MainWindow()
        {

            InitializeComponent();
            if (labels != null)
            {
                PrintOverview();
            }


        }

        /// <summary>
        /// Метод организации выбора анализируемого документа.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChooseFile(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();

            // Интересуют только файлы формата csv
            dlg.Filter = "(*.csv)|*.csv|Все файлы (*.*)|*.*\"";

            if (dlg.ShowDialog() == true)
            {
                analysedFilePath.Text = string.Join(';', dlg.FileNames);
                _analysedDataPath = dlg.FileName;
            }

            // Проверка файла на корректность
            CheckFile();
        }

        /// <summary>
        /// Метод проверки выбранного файла на корректтность.
        /// </summary>
        private void CheckFile()
        {
            try
            {
                using (StreamReader sr = new StreamReader(_analysedDataPath))
                {
                    string data = sr.ReadLine();
                    analyse.IsEnabled = true;

                    // Файл должен начинаться с заголовка свойств,
                    // среди которых обязательно должны быть 
                    // Flow ID, Destination IP, Timestamp, Source IP.
                    if (data.Split(new char[] { ',' },
                        StringSplitOptions.RemoveEmptyEntries).Length < 8 ||
                        !(data.Contains("Flow ID") && data.Contains("Destination IP")
                        && data.Contains("Timestamp") && data.Contains("Source IP")))
                    {
                        MessageBox.Show("Выбранный файл имеет некорректный формат");
                        analysedFilePath.Text = "Выберите файл для анализа";
                        _analysedDataPath = default;
                        analyse.IsEnabled = false;
                    }
                }
            }
            catch (NullReferenceException)
            {
                // Случай пустого файла.
                MessageBox.Show("Выбранный файл оказался пуст");
                analysedFilePath.Text = "Выберите файл для анализа";
                _analysedDataPath = default;
                analyse.IsEnabled = false;
            }
            catch (IOException)
            {
                // Случай ошибки при считывании.
                MessageBox.Show("Произошла ошибка при считывании файла");
                analysedFilePath.Text = "Выберите файл для анализа";
                analysedFilePath.Text = "Выберите файл для анализа";
                _analysedDataPath = default;
                analyse.IsEnabled = false;
            }
            catch (ArgumentNullException)
            {
                // Случай, когда входной файл не выбран.
                MessageBox.Show("Файл для анализа не был выбран");
                analysedFilePath.Text = "Выберите файл для анализа";
                analysedFilePath.Text = "Выберите файл для анализа";
                _analysedDataPath = default;
                analyse.IsEnabled = false;
            }

        }

        /// <summary>
        /// Переход в окно результатов анализа.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnAnalyseResultsClicked(object sender, EventArgs e)
        {
            var ar = new AnalyseResults();
            ar.Show();
            Close();
        }

        private static void AnalyseThread()
        {
            labels = Library.Analyse.PredictIssue(_analysedDataPath);

        }
        public static void InformUserWithEnding()
        {
            MessageBox.Show("Анализ файла окончен");

        }

        /// <summary>
        /// Метод отображения общих данных об атаках.
        /// </summary>
        private void PrintOverview()
        {
            var metrics =
              new { MicroAccuracy = 0.869, MacroAccuracy = 0.833 };

            #region Overview
            summary.Text = $"Анализ файла прошел успешно. {Environment.NewLine}Выявлено {labels.Count()} атак, среди них:" +
                $"{Environment.NewLine}UDP: {labels.FindAll(x => x == "UDP").Count()}" +
                $"{Environment.NewLine}DNS: {labels.FindAll(x => x == "DNS").Count()}" +
                $"{Environment.NewLine}LDAP: {labels.FindAll(x => x == "LDAP").Count()}" +
                $"{Environment.NewLine}MSSQL: {labels.FindAll(x => x == "MSSQL").Count()}" +
                $"{Environment.NewLine}NetBIOS: {labels.FindAll(x => x == "NetBIOS").Count()}" +
                $"{Environment.NewLine}NTP: {labels.FindAll(x => x == "NTP").Count()}" +
                $"{Environment.NewLine}SNMP: {labels.FindAll(x => x == "SNMP").Count()}" +
                $"{Environment.NewLine}SSDP: {labels.FindAll(x => x == "SSDP").Count()}" +
                $"{Environment.NewLine}Portmap: {labels.FindAll(x => x == "Portmap").Count()}" +
                $"{Environment.NewLine}Syn: {labels.FindAll(x => x == "Syn").Count()}" +
                $"{Environment.NewLine}TFTP: {labels.FindAll(x => x == "TFTP").Count()}" +
                $"{Environment.NewLine}UDP-lag: {labels.FindAll(x => x == "UDP-lag").Count()}" +
                $"{Environment.NewLine}Микроточность классификации: {metrics.MicroAccuracy}" +
                $"{Environment.NewLine}Макроточность классификации: {metrics.MacroAccuracy}"
                + $"{Environment.NewLine}Для подробной информации о результатах " +
                $"классификации пройдите в раздел \"Результаты анализа\"";
            #endregion
        }

        /// <summary>
        /// Действия при запуске процесса классификации.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Analyse_Click(object sender, RoutedEventArgs e)
        {
            stop.IsEnabled = true;
            analyse.IsEnabled = false;

            // Составляем делегат из методов для классификации.
            if (thSt == null)
            {
                thSt += AnalyseThread;
                thSt += InformUserWithEnding;
            }
            worker = new Thread(thSt);

            // Запускаем процесс классификации
            worker.Start();

        }

        /// <summary>
        /// Отмена процесса классифкации.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Stop_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                stop.IsEnabled = false;
                analyse.IsEnabled = false;
                Library.Analyse.model.Close();
                MessageBox.Show($"Анализ документа {_analysedDataPath} был отменен пользователем.");

            }
            catch (Exception ex)
            {
                MessageBox.Show($"Возникли проблемы с отменой анализа файла." +
                    $" Детали {ex.Message}");
            }
        }

        /// <summary>
        /// Отображение общих результатов классификации.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Overview_Click(object sender, RoutedEventArgs e)
        {
            if (labels != null)
                PrintOverview();
        }
    }
}